<?php
namespace Goh\Stat\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class Call handle call to stat
 *
 * @package Goh\stat\Controller
 */
class Call
{

    /**
     * Map service name to class name
     *
     * @param $Service
     * @return string|null
     * @author ht@goh.dk ( tholle )
     */
    private static function map($Service = null)
    {
        $map = array(
            "forcast" => "Forcast"
        );

        if (!empty($map[strtolower($Service)]))
        {
            return $map[strtolower($Service)];
        }
        else
        {
            return null;
        }
    }


    /**
     * Check if class method exists
     *
     * @param null $Service - name of nav service
     * @return false|string
     * @author ht@goh.dk ( tholle )
     */
    private static function serviceExist($Service = null)
    {
        $Service = self::map($Service);
        if (!empty($Service))
        {
            $CallName = '\Goh\Stat\Controller\Handler\\' . $Service;
            if (class_exists($CallName) && method_exists(new $CallName, 'handler'))
            {
                return $CallName;
            }
        }

        return false;
    }


    /**
     * Call a statservice with parameters and return the response
     *
     * @param object $Request - Illuminate\Http\Request;
     * @param string $Service - name of the nav service
     * @param object $parameter - argument passed to the service
     */
    public static function Calling(Request $Request, $Service = null, $Parameter = null)
    {
        if (($CallName = self::serviceExist($Service)) !== false)
        {
            try
            {
                $ClassMethod = new \ReflectionMethod($CallName, "handler");
            }
            catch (\ReflectionException $e)
            {
                $ClassMethod = null;
                return $e->getMessage();
            }

            if (is_object($ClassMethod))
            {
                return $ClassMethod->invoke(new $CallName, $Request, $Parameter);
            }
        }

        return null;

    }

}
