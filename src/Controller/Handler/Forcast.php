<?php
namespace Goh\Stat\Controller\Handler;

use Goh\Stat\Controller\BaseController;
use Laravel\Lumen\Http\Request;
use Illuminate\Support\Facades\Log;


Class Forcast extends BaseController
{

    public function handler(Request $Request, $Parameters=null)
    {
        if (!empty($Request->Parameters))
        {
            $Parmeters = explode("/", $Request->Parameters);
            if (count($Parmeters) == 1 && $Parmeters[0] == 'latest')
            {
                return $this->latest($Request);
            }
            elseif (count($Parmeters) == 2 && $Parmeters[0] == 'get' && strtotime($Parmeters[1]))
            {
                return $this->get($Request, $Parmeters[1]);
            }
            elseif (count($Parmeters) == 2 && $Parmeters[0] == 'put' && strtotime($Parmeters[1]))
            {
                return $this->put($Request, $Parmeters[1]);
            }
        }

        return false;
    }


    private function get(Request $Request, $Date)
    {
        if (!empty($Request->account->allow) && is_array($Request->account->allow) && in_array('statcall@forcast@get', $Request->account->allow))
        {
            $Date = strftime("%Y-%m-%d", strtotime($Date));
            $Stat = \App\Models\Stat\Forcast::where('date', $Date)->first();
            if ($Stat)
            {
                return json_decode($Stat->data);
            }
        }

        return array();
    }


    private function put(Request $Request, $Date)
    {
        if (!empty($Request->account->allow) && is_array($Request->account->allow) && in_array('statcall@forcast@put', $Request->account->allow))
        {
            $Date = strftime("%Y-%m-%d", strtotime($Date));

            $json = file_get_contents('php://input');
            if (!empty($json))
            {
                $object = json_decode($json, true);

                if (json_last_error())
                {
                    return json_last_error_msg();
                }
                elseif (is_array($object))
                {
                    $Forcast = \App\Models\Stat\Forcast::where('date', $Date)->first();
                    if (!$Forcast)
                    {
                        $Forcast = new \App\Models\Stat\Forcast();
                        $Forcast->date = $Date;
                    }

                    $Forcast->data = json_encode($object);

                    return $Forcast->save();
                }
            }
        }

        return false;
    }


    private function latest(Request $Request)
    {
        if (!empty($Request->account->allow) && is_array($Request->account->allow) && in_array('statcall@forcast@get', $Request->account->allow))
        {
            $Max = \App\Models\Stat\Forcast::max('date');
            if ($Max)
            {
                $Stat = \App\Models\Stat\Forcast::where('date', $Max)->first();
                if ($Stat)
                {
                    return json_decode($Stat->data);
                }
            }
        }

        return array();
    }

}

